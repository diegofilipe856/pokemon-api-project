import { catchPokeList, verifyPoke } from "../modules_js/modules.js";
import promptSync from 'prompt-sync'
const prompt = promptSync()
const fn = async () =>{
    const stringApiType = "https://pokeapi.co/api/v2/type/"
    let typeOfPokemon = prompt('Digite o tipo da sua lista: ')
    let nameOfPokemon = prompt('Digite o nome do pokemon: ')
    let requisitionLink = stringApiType + typeOfPokemon
    let pokeList = await catchPokeList(requisitionLink)
    const pokeExists = await verifyPoke(pokeList, nameOfPokemon)
    if (pokeExists != undefined){
        console.log(`A URL desse pokemon é : ${pokeExists}`)
    }
    else{
        console.log('Parece que esse pokemon não existe nessa lista. :(')
    }
}

fn();