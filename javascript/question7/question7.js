import { catchPokeList, pokeMatch } from "../modules_js/modules.js"
import PromptSync from "prompt-sync";
const prompt = PromptSync()

const fn = async ()=>{
    const stringApiType = "https://pokeapi.co/api/v2/type/"
    const typeOfPokemon1 = prompt("Qual o primeiro tipo do pokemons? ")
    const typeOfPokemon2 = prompt("Qual o segundo tipo do pokemons? ")
    let requisitionLink1 = stringApiType + typeOfPokemon1
    let requisitionLink2 = stringApiType + typeOfPokemon2
    let pokeList1 = await catchPokeList(requisitionLink1)
    let pokeList2 = await catchPokeList(requisitionLink2)
    const pokeInComum = pokeMatch(pokeList1, pokeList2);
    //console.log(pokeInComum);
    pokeInComum.forEach(element => {
        console.log(`nome: ${element.name}`)
        console.log(`url: ${element.url}`)
        console.log()
    });
}


fn();

