import fetch from "node-fetch";
import promptSync from 'prompt-sync';
const prompt = promptSync();


export const catchPokeList = async (requisitionLink)=>{
    const getJson = await fetch(requisitionLink)
    let poke = await getJson.json()
    poke = poke.pokemon
    const names = poke.map(poke => poke.pokemon.name)
    const urls = poke.map(poke => poke.pokemon.url)
    let arrayOfPokemons =[]
    for (let i = 0; i<poke.length;i++){
        let tempObj = {}
        tempObj.name = names[i]
        tempObj.url = urls[i]
        arrayOfPokemons.push(tempObj)
    }
    //console.log(poke)
    //console.log("Pokemon: ")
    //console.log(`nome: ${element.pokemon.name}`)
    //console.log(`url: ${element.pokemon.url}`)
    //console.log(names)
    //console.log(urls)
    //console.log(arrayOfPokemons)
    return arrayOfPokemons
    };
    
    export const verifyPoke = async (pokeList, pokeName) =>{
    let urlOfChosen;
        pokeList.forEach(element => {
        if (element.name == pokeName){
            console.log('O pokemon existe nessa lista')
            urlOfChosen = element.url 

        }
    });
        if (urlOfChosen != undefined){    
            return urlOfChosen
    }
}

export const addNewPoke = (pokeList, newPokemon) =>{
    let tempDict = {}
    tempDict.name = newPokemon
    tempDict.url = prompt('Digite a URL desse pokemon: ')
    pokeList.push(tempDict)
    return pokeList;
    };


export const deletePokemon = (pokeList, toBeDeleted) =>{
    let wasDeleted;
    pokeList.forEach(element => {
        if (element.name == toBeDeleted){
            pokeList.pop(element.name)
            pokeList.pop(element.url)
            wasDeleted = true
        }
        
    })
    if (wasDeleted == true){
        return `O pokemon ${toBeDeleted} foi deletado.`
    }
    else{
        return `O pokemon não existe, portanto não foi deletado.`
    };
}

export const changePoke = (pokeList, pokeThatExists, newPokeName) =>{
    pokeList.forEach(element => {
        if (element.name == pokeThatExists){
            element.name = newPokeName
        }
    });
    return pokeList
}


export const compareCrescent = (a, b)=>{
    if (a.name < b.name){
        return -1;
    }
    else if (a.name > b.name){
        return 1;
    }
    else{
        return 0;
    }
}

export const compareDerescent = (a, b)=>{
    if (a.name < b.name){
        return 1;
    }
    else if (a.name > b.name){
        return -1;
    }
    else{
        return 0;
    }
}

export const sortList = (pokeList, opcao)=>{
    if (opcao == 'crescente'){
        pokeList.sort(compareCrescent)
        return pokeList
    }
    else if (opcao == 'decrescente'){
        pokeList.sort(compareDerescent)
        return pokeList
    }
    else{
        return 'Opção de ordenação inválida. '
    }
}

export const pokeMatch = (pokeList1, pokeList2)=>{
    
    let arrayOfMatchPoke = []
    pokeList1.forEach(element => {
        pokeList2.forEach(element2=> {
            if (element2.name == element.name){
                let tempDict = {}
                tempDict.name = element.name
                tempDict.url = element.url
                arrayOfMatchPoke.push(tempDict)
            } 
        });
    });
    return arrayOfMatchPoke
}

export const pokeClue = (pokeList1, pokeList2)=>{
    
    let arrayOfClue = []
    pokeList1.forEach(element => {
        if (element in pokeList2 ==false){
            let tempDict = {}
            tempDict.name = element.name
            tempDict.url = element.url
            arrayOfClue.push(tempDict)
        }});
        return arrayOfClue
    };
    
