import { catchPokeList, deletePokemon } from "../modules_js/modules.js";
import PromptSync from "prompt-sync"

const prompt = PromptSync();

const fn = async ()=>{
const stringApiType = "https://pokeapi.co/api/v2/type/"
const typeOfPokemon = prompt("Qual o tipo do pokemon? ")
let requisitionLink = stringApiType + typeOfPokemon
let pokeList = await catchPokeList(requisitionLink)
let toBeDeleted = prompt("Digite o nome do pokemon a ser deletado: ")
const deletePoke = deletePokemon(pokeList, toBeDeleted);
console.log(deletePoke)
}
fn();