import {catchPokeList} from "../modules_js/modules.js"


const stringApiType = "https://pokeapi.co/api/v2/type/"
let typeOfPokemon = "rock"
let requisitionLink = stringApiType + typeOfPokemon

const fn = async () =>{
const pokeList = await catchPokeList(requisitionLink)
pokeList.forEach(element => {
    console.log(`nome: ${element.name}`)
    console.log(`url: ${element.url}`)
    console.log()
});
}
fn();