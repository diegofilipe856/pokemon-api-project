import { addNewPoke, catchPokeList, verifyPoke } from "../modules_js/modules.js";
import PromptSync from "prompt-sync";
const prompt = PromptSync()

const fn = async ()=>{
    const stringApiType = "https://pokeapi.co/api/v2/type/"
    const typeOfPokemon = 'rock'
    let requisitionLink = stringApiType + typeOfPokemon
    let pokeList = await catchPokeList(requisitionLink)
    let newPokemon = prompt("Digite o nome do novo pokemon: ")
    const urlOfPoke = await verifyPoke(pokeList, newPokemon)
    if (urlOfPoke == undefined){
        let newPokeList = await addNewPoke(pokeList, newPokemon)
        pokeList.forEach(element => {
            console.log(`nome: ${element.name}`)
            console.log(`url: ${element.url}`)
            console.log()
        });
    }}
fn();
