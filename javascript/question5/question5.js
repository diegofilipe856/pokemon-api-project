import { catchPokeList, changePoke } from "../modules_js/modules.js"
import PromptSync from "prompt-sync"

const fn = async ()=>{
    const prompt = PromptSync()
    const stringApiType = "https://pokeapi.co/api/v2/type/"
    const typeOfPokemon = prompt("Qual o tipo do pokemon? ")
    let requisitionLink = stringApiType + typeOfPokemon
    let pokeList = await catchPokeList(requisitionLink)
    let pokeThatExists = prompt("Digite o nome do pokemon que você quer mudar o nome: ")
    let newPokeName = prompt(`Digite o novo nome de ${pokeThatExists}: `)
    const newList = changePoke(pokeList, pokeThatExists, newPokeName)
    newList.forEach(element => {
        console.log(`nome: ${element.name}`)
        console.log(`url: ${element.url}`)
        console.log()
    });
}

fn();