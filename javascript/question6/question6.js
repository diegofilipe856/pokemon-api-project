import PromptSync from "prompt-sync"
import { catchPokeList, sortList } from "../modules_js/modules.js"

const prompt = PromptSync()

const fn = async()=>{
    
    const stringApiType = "https://pokeapi.co/api/v2/type/"
    const typeOfPokemon = prompt("Qual o tipo do pokemon? ")
    let requisitionLink = stringApiType + typeOfPokemon
    let pokeList = await catchPokeList(requisitionLink)
    const opcao = prompt("Qual a ordem? (crescente/decrescente) ")
    const orderedPokeList = sortList(pokeList, opcao)
    pokeList.forEach(element => {
        console.log(`nome: ${element.name}`)
        console.log(`url: ${element.url}`)
        console.log()
    });
}

fn();