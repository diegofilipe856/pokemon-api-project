import modules_py

type_of_pokemon = input("Digite o tipo dos pokemons: ")
poke_list = modules_py.do_url(type_of_pokemon)
new_poke = {"pokemon":{}}
new_poke["pokemon"]["name"] = input("Digite o nome do pokemon: ")
new_poke_list = modules_py.add_poke(poke_list, new_poke)
if new_poke in poke_list:
    modules_py.show_pokemon(poke_list)
