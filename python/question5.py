from modules_py import *


type_of_pokemon = input("Digite o tipo dos pokemons: ")
poke_list = do_url(type_of_pokemon)
poke_that_exist = input("Digite o nome do pokemon que já existe e deve ser trocado: ")
new_poke_name = input("Digite o novo nome para o pokemon: ")
new_poke_list = replace_name(poke_list, poke_that_exist, new_poke_name)
show_pokemon(new_poke_list)