from modules_py import *


type_of_pokemon = input("Digite o tipo dos pokemons: ")
poke_list = do_url(type_of_pokemon)
orientation = input("Digite a ordenação (crescente/decrescente): ")
new_poke_list = sort_list(poke_list, orientation)
if new_poke_list != 'erro':
    show_pokemon(poke_list)