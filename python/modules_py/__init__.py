import requests

def do_url(type_of_poke):
    string_of_api = 'https://pokeapi.co/api/v2/type/'
    url = string_of_api + type_of_poke
    url = requests.get(url).json()
    poke_list = url['pokemon']
    return poke_list

def show_pokemon(poke_list):
    for i in poke_list:
        print(i['pokemon']['name'])
        print(i['pokemon']['url'], '\n')

def poke_exists(poke_list, poke_name):
    existing = ''
    for i in range(0, len(poke_list)):
        if poke_name == poke_list[i]['pokemon']['name']:
            print('O pokemon existe nessa lista. ')
            existing = poke_list[i]['pokemon']['url']
    if existing != '':
        return existing
    else:
        return 'O pokemon não existe nessa lista'

def add_poke(poke_list, new_poke):
    poke_already_exists = poke_exists(poke_list, new_poke["pokemon"]["name"])
    print(poke_already_exists)
    if poke_already_exists == 'O pokemon não existe nessa lista':
        new_poke["pokemon"]["url"] = input("Digite a url do pokemon: ")
        poke_list.append(new_poke)
    else:
        print('A adição não pode ser feita pois iria duplicar os pokemons.')
    return poke_list

def delete_poke(poke_list, poke_tobe_deleted):
    for i in range(0, len(poke_list)):
        if poke_list[i]["pokemon"]["name"] == poke_tobe_deleted:
            del(poke_list[i])
            break
    return poke_list

def replace_name(poke_list, poke_that_exist, new_poke_name):
    for i in range(0, len(poke_list)):
        if poke_list[i]["pokemon"]["name"] == poke_that_exist:
            poke_list[i]["pokemon"]["name"] = new_poke_name
    return poke_list

def sort_list(poke_list, orientation):
    if orientation == 'crescente':
        poke_list.sort(reverse=False, key=reference_sort)
        return poke_list
    elif orientation == 'decrescente':
        poke_list.sort(reverse=True, key=reference_sort)
        return poke_list
    else:
        print('Opção inválida')
        return 'erro'

    

def reference_sort(poke_list):
    return poke_list["pokemon"]["name"]


def pokematch(poke_list1, poke_list2):
    array_of_matching_poke = []
    for i in poke_list1:
        for j in poke_list2:
            if j["pokemon"]["name"] == i["pokemon"]["name"]:
                temp_dict = {"pokemon":{}}
                temp_dict["pokemon"]["name"] = j["pokemon"]["name"]
                temp_dict["pokemon"]["url"] = j["pokemon"]["url"]
                array_of_matching_poke.append(temp_dict)
    return array_of_matching_poke

def poke_clue(poke_list1, poke_list2):
    array_of_clue = []
    for i in range(0, poke_list1):
        if poke_list[i] not in poke_list2:
            array_of_clue.append(poke_list[i])
    return array_of_clue
